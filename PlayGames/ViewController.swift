//
//  ViewController.swift
//  PlayGames
//
//  Created by 1 on 17.09.2018.
//  Copyright © 2018 mavka.in.ua. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let stepSuccess = NSNotification.Name(rawValue: "stepSuccess")
let youAreWin = NSNotification.Name(rawValue: "youAreWin")
let youAreLoos = NSNotification.Name(rawValue: "youAreLoos")
var apitoken :String = ""

class ViewController: UIViewController {
    
    @IBOutlet weak var BoardView: UIView!
    
    @IBOutlet weak var commentLabel: UILabel!
    let myChess = ChessGame.init()
    
    @IBAction func myBoardTap(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: BoardView)
        myChess.getTap(location: location, view: BoardView)
    }
    
    // let board = [OneField]
    
    let size :Int = 32
    override func viewDidLoad() {
        super.viewDidLoad()
        apitoken  = UserDefaults.standard.string(forKey: "apitoken") ?? ""
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(foo(notification:)),
                                               name: stepSuccess,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(theEnd(notification:)),
                                               name: youAreWin,
                                               object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(theEnd(notification:)),
                                               name: youAreLoos,
                                               object: nil)

        
       // showChessBoard(6)
        myChess.drawBoard(view: BoardView)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Get Token
        print ("touchBegan: apitoken \(apitoken)")
        let currentHost = UIDevice.current.name
        let sysName = UIDevice.current.identifierForVendor!.uuidString
           // NSHost.current().localizedName ?? ""
        print ("Hostname \(currentHost); sysname - \(sysName)")
        if apitoken.isEmpty {
            let tmp = GetTokenRequest()
            print ("apitoken is Empty. new apitoken \(tmp) ")
            // login if username is not empty
            // else register
             UserDefaults.standard.set(apitoken, forKey: "apitoken")
        } else {
          //  let tmp = SendToken()
        }
        
    }

    
    @objc func theEnd(notification: Notification) {
      guard let userInfo = notification.userInfo,
        let status = userInfo["status"] as? String,
        let player = userInfo["player"] as? Int else {
            return
        }
        print ("user \(player) is \(status)")
    }
    

    
    @objc func foo(notification: Notification) {
        let mapArray = myChess.myBoard.map({ String($0.col) + "|" + String($0.row) + "|" + String($0.isDark) + "|" + String($0.figureType) + "|" + String($0.figureColor) })
        let mapString = mapArray.joined(separator: "-")
  //      print ("map 1 \(mapString)")
 //       sendMapRequest(iduser: myChess.selfColor, idgame: 1, map: mapString)
        if myChess.selfColor == 1 {
            if myChess.countPeshki(2) == 0 {
                // you are WIN
                print("1 is win")
            }
            if myChess.countPeshki(1) == 0 {
                // you are LOOs
                print("1 is loos")
            }
            
            myChess.selfColor = 2
        } else {
            if myChess.countPeshki(1) == 0 {
                // you are WIN
                print("2 is win")
            }
            if myChess.countPeshki(2) == 0 {
                // you are LOOs
                print("2 is loos")
            }
            
            myChess.selfColor = 1
        }
        commentLabel.text = "ХОД ИГРОКА \(myChess.selfColor) "
        
        let map2Array = mapString.components(separatedBy: "-")
        let newBoard :[ChessGame.OneField] = map2Array.map({
//            print("MAP2!!!! \($0)")
            let tmpArray = $0.components(separatedBy: "|")
            
            let col = Int(tmpArray[0]) ?? 0
            let row = Int(tmpArray[1]) ?? 0
            let isDark = Bool(tmpArray[2]) ?? true
            let figureType = Int(tmpArray[3]) ?? 0
            let figureColor = Int(tmpArray[4]) ?? 0
            
            let tmpField :ChessGame.OneField = ChessGame.OneField(row: row, col: col, isDark: isDark, figureType: figureType,  figureColor: figureColor)
            return tmpField
        })
        myChess.myBoard = newBoard
        myChess.drawBoard(view: BoardView)
  //      winGame()
    }

    
    func didTap(sender: UITapGestureRecognizer) {
        let location = sender.location(in: view)
        print ("X \(location.x) Y \(location.y)")
        // User tapped at the point above. Do something with that if you want.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func GetTokenRequest() -> String {
        /**
         GetToken
         post http://91.211.118.196:8000/api/login
         */
        
        // Add Headers
        let headers = [
            "Accept":"application/json",
            "Content-Type":"application/json; charset=utf-8",
            ]
        
        // JSON Body
        let body: [String : Any] = [
            "email": "mavka@chado.in.ua",
            "password": "ChangeIT"
        ]
        var result :String = ""
        
        // Fetch Request
        Alamofire.request("http://91.211.118.196:8000/api/login", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    
                    // 1
                    guard response.result.isSuccess,
                        let value = response.result.value else {
                            print("Error while uploading file: \(String(describing: response.result.error))")
                            return
                    }
                    
                    // 2
                    result = JSON(value)["data"]["api_token"].string ?? ""
                    //result = "OK"
                    print("Content uploaded with ID: \(value) -  \(result)")
                    UserDefaults.standard.set(result, forKey: "apitoken")
                }
                else {
                    debugPrint("HTTP Request failed: \(response.result.error)")
                }
        }
        return result
    }
    
    
    func sendMapRequest(iduser :Int, idgame :Int, map :String) {
        /**
         NewGame
         post http://91.211.118.196:8000/api/step
         */
        print ("MAP \(map)")
        // Add Headers
        let headers = [
            "Accept":"application/json",
            "Content-Type":"application/json; charset=utf-8",
            ]
        
        // JSON Body
        let body: [String : Any] = [
            "idgame": "1",
            "iduser": iduser,
            "map": map
        ]
        
        // Fetch Request
        Alamofire.request("http://91.211.118.196:8000/api/step?api_token=\(UserDefaults.standard.string(forKey: "apitoken")!)", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    debugPrint("HTTP Response Body: \(response.data)")
                }
                else {
                    debugPrint("HTTP Request failed: \(response.result.error)")
                }
        }
    }
    
    

    

    func SendToken() -> String {
        /**
         SendToken
         get http://91.211.118.196:8000/api/user
         */
        
        // Add Headers
        let headers = [
            "Accept":"application/json",
            "Content-Type":"application/json; charset=utf-8",
            ]
        
        
        // JSON Body
     //   let body: [String : Any] = []
        
        var result :String = ""
        
        // Fetch Request
        Alamofire.request("http://91.211.118.196:8000/api/user?api_token=\(apitoken)", method: .get,  encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
    
                     debugPrint("HTTP Response Body: \(response.data)")
                    // 1
                    guard response.result.isSuccess,
                        let value = response.result.value else {
                            print("Error while uploading file: \(String(describing: response.result.error))")
                            return
                    }

                    // 2
                    result = JSON(value)["data"]["code"].string ?? ""
             //       result = "OK"
                    print("Content uploaded with ID: \(value) -  \(result)")

                
                }
                else {
                    debugPrint("HTTP Request failed: \(response.result.error)")
                }
        }
        return result
    }
    
    

    
    
    
}

