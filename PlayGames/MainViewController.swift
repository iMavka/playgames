//
//  MainViewController.swift
//  PlayGames
//
//  Created by 1 on 25.09.2018.
//  Copyright © 2018 mavka.in.ua. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBAction func goToChess(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showChess", sender: sender)
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}
