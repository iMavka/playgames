//
//  ChessGame.swift
//  PlayGames
//
//  Created by 1 on 17.09.2018.
//  Copyright © 2018 mavka.in.ua. All rights reserved.
//

import UIKit


class ChessGame: NSObject {
    
    let size :Int = 32
    let count :Int = 8
    var selfColor = 1
    var isItBoi :Bool = false
    
    struct OneField {
        var row :Int = -1
        var col :Int = -1
        var isDark :Bool = true
        var figureType :Int = -1
        var figureColor :Int = -1
        
    }
    
    var fromField = OneField()
    var toField = OneField()
    
    var myBoard :[OneField] = []
    
    let startX :Int = 10
    let startY :Int = 10
    
    let figureColor : [CGColor] = [
        UIColor.white.cgColor,
        UIColor.red.cgColor,
        UIColor.blue.cgColor
    ]
    
    override init () {
        super.init()
        self.initNewBoard()
    }
    
    func getTap (location :CGPoint, view: UIView)
    {
        let myX = Int( (Int(location.x) - startX) / size )
        let myY = count - Int( (Int(location.y) - startY) / size )
        let myPoint = CGPoint.init(x: myX, y: myY)
        var myField = getField(location: myPoint)
        if fromField.col == -1 && myField.figureType > 0  && myField.figureColor == selfColor {
            fromField = myField
            drawField(myField: fromField, view: view)
            myField.col = -1
        }
        if fromField.col == myField.col && fromField.row == myField.row && !isItBoi {
            fromField.col = -1
            drawField(myField: myField, selected: false, view: view)
        }
        if fromField.col > -1 {
            toField = myField
            makeStep(view: view)
        }
    }
    
    func makeStep (view: UIView)
    {
        //   var tmpBoard :[OneField] = []
        var stepIsOk : Bool = false
        let test = abs((fromField.col - toField.col )*(fromField.row - toField.row))
        
        if test == 1 && toField.figureType == 0 {
            stepIsOk = simpleStep(view: view)
        } // prosto step
        
        if toField.figureType == 0 && fromField.figureType == 2 &&
            abs(fromField.col - toField.col) == abs(fromField.row - toField.row) {
            stepIsOk = damkaStep(view:view)
            print ("damka step")
        }
        
        if test == 4 && toField.figureType == 0 {
            stepIsOk = boiStep(view: view)
        } // pobulu  peshku vraga
        //        print ("From \(fromField.col) \(fromField.row) - TO \(toField.col) \(toField.row)")
  
        if isItBoi && fromField.col == toField.col && fromField.row == toField.row {
            isItBoi = false
            stepIsOk = true
        }
 //       print ("StepIsOk - \(stepIsOk) isItBoy = \(isItBoi)")
        
        if stepIsOk  && !isItBoi {
            drawBoard(view: view)
            fromField.col = -1
            fromField.row = -1
            fromField.figureType = 0
            var otherColor = 1
            if otherColor == selfColor {
                otherColor = 2
            }
            if countPeshki(otherColor) == 0 {
                NotificationCenter.default.post(name: youAreWin,
                                                object: self,
                                                userInfo: ["status": "winner", "player": selfColor])
                NotificationCenter.default.post(name: youAreLoos,
                                                object: self,
                                                userInfo: ["status": "loos", "player": otherColor])
                
                
            }
            NotificationCenter.default.post(name: stepSuccess,
                                            object: self,
                                            userInfo: nil)
        }
    }
    
    func countPeshki (_ color: Int = 1) -> Int {
        var result = 0
        for i in 0..<64  {
            if myBoard[i].figureType > 0 && myBoard[i].figureColor == color {
                result += 1
            }
        }
        return result
    }
    
    func damkaStep (view: UIView) -> Bool {
        var result :Bool = true
        var countPeshka = 0
        var rowRost = 1
        var colRost = 1
        if fromField.row > toField.row {
            rowRost = -1
        }
        if fromField.col > toField.col {
            colRost = -1
        }
        var tmpField = fromField
        var vragField = fromField
 //       print ("From \(fromField.col) \(fromField.row) TO \(toField.col) \(toField.row)")
        while tmpField.col != toField.col {
            tmpField.col += colRost
            tmpField.row += rowRost
            let myPoint = CGPoint.init(x: tmpField.col, y: tmpField.row)
            let myField = getField(location: myPoint)
 //           print ("myField \(myField.col) \(myField.row) \(myField.figureColor) \(myField.figureType)")
            if myField.figureColor == selfColor  && myField.figureType > 0 {
                result = false
            }
            if myField.figureColor != selfColor && myField.figureType > 0 {
                countPeshka += 1
                vragField = myField
            }
        }
        if countPeshka > 1 {
            result = false
        }
        if result {
            for i in 0..<64  {
                tmpField=myBoard[i]
                if tmpField.col == fromField.col && tmpField.row == fromField.row {
                    myBoard[i].figureType = 0
                    drawField(myField: myBoard[i], selected: false, view: view)
                    
                }
                if tmpField.col == toField.col && tmpField.row == toField.row {
                    myBoard[i].figureType = fromField.figureType
                    myBoard[i].figureColor = fromField.figureColor
                    drawField(myField: myBoard[i], selected: false, view: view)
                    
                }
                if countPeshka == 1 && vragField.col == tmpField.col && vragField.row == tmpField.row {
                    myBoard[i].figureColor = 0
                    myBoard[i].figureType = 0
                    drawField(myField: myBoard[i], selected: false, view: view)
                    
                    isItBoi = true
                }
            }
            if isItBoi {
                fromField.col  = toField.col
                fromField.row = toField.row
                drawField(myField: fromField, selected: true, view: view)
                
                toField.col = -1
            } else {
                fromField.col = -1
            }
        }
        
        return result
    }
    
    func boiStep (view: UIView) -> Bool {
        var result :Bool = false
        
        // фишка противника
        let tmpCol = toField.col > fromField.col ? fromField.col + 1 : toField.col + 1
        let tmpRow = toField.row > fromField.row ? fromField.row + 1 : toField.row + 1
        let myPoint = CGPoint.init(x: tmpCol, y: tmpRow)
        let myField = getField(location: myPoint)
        
        if (myField.figureColor != selfColor && myField.figureType > 0)
        {
            result = true
            isItBoi = true
            var tmpField = fromField
            for i in 0..<64  {
                tmpField=myBoard[i]
                if tmpField.col == fromField.col && tmpField.row == fromField.row {
                    myBoard[i].figureType = 0
                    drawField(myField: myBoard[i], selected: false, view: view)
                }
                if tmpField.col == toField.col && tmpField.row == toField.row {
                    myBoard[i].figureType = fromField.figureType
                    myBoard[i].figureColor = fromField.figureColor
                    drawField(myField: myBoard[i], selected: false, view: view)
                    
                }
                if tmpField.col == myField.col && tmpField.row == myField.row {
                    myBoard[i].figureType = 0
                    myBoard[i].figureColor = 0
                    drawField(myField: myBoard[i], selected: false, view: view)
                    
                }
            }
            fromField.col = toField.col
            fromField.row = toField.row
            drawField(myField: fromField, selected: true, view: view)
            
            toField.col = -1
        }
        return result
        
    }
    
    func simpleStep (view: UIView) -> Bool {
        var tmpField = fromField
        var result :Bool = false
        if (fromField.figureColor == 1 && toField.row > fromField.row)
        {
            result = true
        }
        if (fromField.figureColor == 2 && toField.row < fromField.row)
        {
            result = true
        }
        
        if result {
            for i in 0..<64  {
                tmpField=myBoard[i]
                if tmpField.col == fromField.col && tmpField.row == fromField.row {
                    myBoard[i].figureType = 0
                }
                if tmpField.col == toField.col && tmpField.row == toField.row {
                    myBoard[i].figureType = fromField.figureType
                    myBoard[i].figureColor = fromField.figureColor
                }
            }
            fromField.col = -1
        }
        return result
    }
    
    func getField (location :CGPoint) -> OneField {
        var result :OneField = myBoard[0]
        for result1 in myBoard {
            if result1.col == Int(location.x) && result1.row == Int(location.y) {
                result=result1
                break
            }
        }
        return (result)
    }
    
    
    func drawBox(x: Int, y: Int, color: UIColor = UIColor.darkGray, view: UIView) {
        let rect = CGRect.init(x: x, y: y, width: size, height: size)
        let box=UIView.init(frame: rect)
        box.backgroundColor =  color
        view.addSubview(box)
    }
    
    func drawBoard (view: UIView) {
        for myField  in myBoard {
            drawField(myField: myField, selected: false, view: view)
        }
    }
    
    
    
    func drawField (myField : OneField, selected :Bool = true, view: UIView) {
        let x = startX + myField.col * size
        let y = startY + (count-myField.row) * size
        var myColor = UIColor.lightGray
        if myField.isDark {
            myColor = UIColor.darkGray
        }
        if selected {
            myColor = UIColor.white
        }
        drawBox(x: x, y: y, color: myColor, view: view)
        if myField.figureType == 1 {
            drawCircle(centerX: Int(x+size/2), centerY: Int(y+size/2), radius: Int(size/3), color: figureColor[myField.figureColor], view: view)
        }
        if myField.figureType == 2 {
            drawCircleSpecial(centerX: Int(x+size/2), centerY: Int(y+size/2), radius: Int(size/3), color: figureColor[myField.figureColor], view: view)
        }
        
    }
    

    
    func drawCircle (centerX: Int, centerY: Int, radius: Int, color: CGColor, view: UIView) {
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: centerX,y: centerY), radius: CGFloat(radius), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = color
        view.layer.addSublayer(shapeLayer)
    }
    
    func drawCircleSpecial (centerX: Int, centerY: Int, radius: Int, color: CGColor, view: UIView) {
        var circlePath = UIBezierPath(arcCenter: CGPoint(x: centerX,y: centerY), radius: CGFloat(radius), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        var shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = color
        view.layer.addSublayer(shapeLayer)
        
        circlePath = UIBezierPath(arcCenter: CGPoint(x: centerX,y: centerY), radius: CGFloat(radius/2), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.white.cgColor
        view.layer.addSublayer(shapeLayer)
    }
    
    
    
    func initNewBoard () {
        var myField : OneField
        
        myField = OneField(row: 0, col: 0, isDark: true, figureType: 2, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 0, col: 1, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 0, col: 2, isDark: true, figureType: 2, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 0, col: 3, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 0, col: 4, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 0, col: 5, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 0, col: 6, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 0, col: 7, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        
        
        
        
        myField = OneField(row: 1, col: 0, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 1, col: 1, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 1, col: 2, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 1, col: 3, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 1, col: 4, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 1, col: 5, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 1, col: 6, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 1, col: 7, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        
        
        myField = OneField(row: 2, col: 0, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 2, col: 1, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 2, col: 2, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 2, col: 3, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 2, col: 4, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 2, col: 5, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 2, col: 6, isDark: true, figureType: 1, figureColor: 1)
        myBoard.append(myField)
        myField = OneField(row: 2, col: 7, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        
        myField = OneField(row: 3, col: 0, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 3, col: 1, isDark: true, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 3, col: 2, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 3, col: 3, isDark: true, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 3, col: 4, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 3, col: 5, isDark: true, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 3, col: 6, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 3, col: 7, isDark: true, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        
        
        myField = OneField(row: 4, col: 0, isDark: true, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 4, col: 1, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 4, col: 2, isDark: true, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 4, col: 3, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 4, col: 4, isDark: true, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 4, col: 5, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 4, col: 6, isDark: true, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 4, col: 7, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        
        myField = OneField(row: 5, col: 0, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 5, col: 1, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 5, col: 2, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 5, col: 3, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 5, col: 4, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 5, col: 5, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 5, col: 6, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 5, col: 7, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        
        
        myField = OneField(row: 6, col: 0, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 6, col: 1, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 6, col: 2, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 6, col: 3, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 6, col: 4, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 6, col: 5, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 6, col: 6, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 6, col: 7, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        
        
        
        myField = OneField(row: 7, col: 0, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 7, col: 1, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 7, col: 2, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 7, col: 3, isDark: true, figureType: 1, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 7, col: 4, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 7, col: 5, isDark: true, figureType: 2, figureColor: 2)
        myBoard.append(myField)
        myField = OneField(row: 7, col: 6, isDark: false, figureType: 0, figureColor: 0)
        myBoard.append(myField)
        myField = OneField(row: 7, col: 7, isDark: true, figureType: 2, figureColor: 2)
        myBoard.append(myField)
        
    }
    
}
